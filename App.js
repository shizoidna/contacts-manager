import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import ContactHome from './screens/ContactHome';
import { createStackNavigator } from '@react-navigation/stack';
import ContactDetails from './screens/ContactDetails';
import { StatusBar } from 'react-native';

const Stack = createStackNavigator();

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content"/>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={ContactHome} options={{ title: 'My Contacts' }}/>
        <Stack.Screen name="Details" component={ContactDetails}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};


export default App;
