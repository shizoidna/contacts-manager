import React from 'react';
import { Text, View, TextInput, StyleSheet, Image, TouchableOpacity } from 'react-native';

class ContactHome extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput/>
        <View styles={styles.wrapper}>
          <View>
            <Image styles={styles.image}/>
            <View styles={styles.userInfoBlock}>
              <Text styles={styles.userName}>Name</Text>
              <Text styles={styles.userEmail}>Email</Text>
              <TouchableOpacity styles={styles.btn}>
                <Text styles={styles.btnText}>My button</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  wrapper: {
    flex: 1
  },
  btn: {
    backgroundColor: '#a5a4a4',
    borderRadius: 60,
    flex: 1,
    height: 30,
    width: 30
  },
  userInfoBlock: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  userName: {
    flex: 1,
    width: 50,
    height: 50
  },
  image: {
    flex: 1,
    backgroundColor: '#000',
    width: 50,
    height: 50,
  },
  userEmail: {
    width: 50,
    height: 50
  },
  btnText: {
    width: 50,
    height: 50
  }
})

export default ContactHome;
